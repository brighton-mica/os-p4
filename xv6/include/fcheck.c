#include <stdio.h>
#include <sys/types.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <assert.h>
#include <stdbool.h>

#include "types.h"
#include "fs.h"

#define BLOCK_SIZE (BSIZE)

#define T_DIR  1   // Directory
#define T_FILE 2   // File
#define T_DEV  3   // Special device

#define CHECK(val, msg) do { \
  if (val == false)           \
  {                           \
    fprintf(stderr,msg);              \
    exit(1);                  \
  }                           \
} while(0)                    \

struct dinode* getInodePtr(const char* addr, const uint num)
{
  return (struct dinode*) (addr + IBLOCK(num)*BLOCK_SIZE + sizeof(struct dinode) * (num % IPB));
}

void printByte(const uchar byte)
{
  uint i=0;
  for (i = 0; i < 8; ++i)
  {
    int output = (byte >> i) & 1 ? 1 : 0;
    printf("%d", output);
  }
  printf("\n");
}



int
main(int argc, char *argv[])
{
  int i,n,fsfd;
  char *addr;
  struct dinode *dip;
  struct superblock *sb;
  struct dirent *de;
  struct stat fileStat;

  if(argc < 2){
    fprintf(stderr, "Usage: sample fs.img ...\n");
    exit(1);
  }


  fsfd = open(argv[1], O_RDONLY);
  if(fsfd < 0){
    perror(argv[1]);
    exit(1);
  }
  
  //get file stat
    if(fstat(fsfd,&fileStat) < 0){
        exit(1);
    }

  /* Dont hard code the size of file. Use fstat to get the size */
  addr = mmap(NULL, fileStat.st_size, PROT_READ, MAP_PRIVATE, fsfd, 0);
  if (addr == MAP_FAILED){
	perror("mmap failed");
	exit(1);
  }

  dip = (struct dinode *) (addr + IBLOCK((uint)0)*BLOCK_SIZE); 

  /* read the super block */
  sb = (struct superblock *) (addr + 1 * BLOCK_SIZE);

  const uint numBlocks = sb->nblocks;
  const uint numInodes = sb->ninodes;

  //const uint firstBitmapBlock = BBLOCK(0, numInodes);

  bool* inodeInUseDataBlocksBitfield = (bool*)(malloc(sizeof(bool) * numBlocks));
  uint* inodeInUseDirectoryBitfield = (uint*)(malloc(sizeof(uint) * numInodes));

  uint* numTimesFileIsSeenInDirectories = (uint*)(malloc(sizeof(uint) * numInodes));

  // Init bitmasks to empty state
  {
    uint i;
    for (i = 0; i < numBlocks; ++i)
    {
      inodeInUseDataBlocksBitfield[i] = 0;
    }

    for (i = 0; i < numInodes; ++i)
    {
      inodeInUseDirectoryBitfield[i] = 0;
      numTimesFileIsSeenInDirectories[i] = 0;
    }
  }




  struct dinode* rootInode = getInodePtr(addr, ROOTINO);

  // Test Case 3 - Root Directory Integrity (badroot, badroot2)
  {
    // Validate that inode exists
    CHECK((rootInode->type != 0), "ERROR: bad inode.\n");

    struct dirent* rootDir = (struct dirent*) (addr + (rootInode->addrs[0]) * BLOCK_SIZE);

    bool selfCorrect, parentCorrect = false;
    const uint maxDirs = rootInode->size / sizeof(struct dirent);
    uint i;
    for (i = 0; i < maxDirs && (!selfCorrect || !parentCorrect); ++i)
    {
      const struct dirent dirEntry = rootDir[i];

      if (dirEntry.inum == 0) 
        continue;
       
      if (strcmp(dirEntry.name, ".") == 0)
      {
        // Validate if root dir's if parent dir is pointing to itself
        const bool valid = dirEntry.inum == 1;
        CHECK(valid, "ERROR: root directory does not exist.\n");
        selfCorrect = true;
      }
      
      if (strcmp(dirEntry.name, "..") == 0)
      {
        // Validate if root dir's if parent dir is pointing to itself
        const bool valid = dirEntry.inum == 1;
        CHECK(valid, "ERROR: root directory does not exist.\n");
        parentCorrect = true;
      }
    }

    // Validate if root dir and parent dir even exist
    CHECK((selfCorrect && parentCorrect), "ERROR: root directory does not exist.\n");
  }
  uint inodeNum;
  for (inodeNum = ROOTINO; inodeNum < numInodes; ++inodeNum)
  {
    struct dinode* inode = getInodePtr(addr, inodeNum); 
    if (inode->type != 0)
    {
      // Test Case 1 - Valid Inodes (badaddr)
      {
        const bool valid = inode->type == T_DIR || inode->type == T_FILE || inode->type == T_DEV;
        CHECK(valid, "ERROR: bad inode.\n");
      }

      // Test Case 2, 5 - Inodes Pointing to Valid Data Blocks (badindir1, badindir2. mrkfree, indirfree)
      {
        bool doneProcessing = false;
        bool selfValidated = false;
        bool parentValidated = false;
        uint i;
        for (i = 0; i < NDIRECT && !doneProcessing; ++i)
        {
          const uint blockIdx = inode->addrs[i];

          if (blockIdx == 0)
          {
            doneProcessing = true;
            break;
          }

          // Validate if in range
          CHECK((blockIdx < numBlocks), "ERROR: bad direct address in inode.\n");

          const uint bitmapBlockIdx = BBLOCK(blockIdx, numInodes);
          const uint localBitmapBlockIdx = (blockIdx % BPB) / 8; // can only read min 1 byte at a time, so divide by 8 and check against bitmask
          const uchar bitfield = 1 << (blockIdx % 8);
          const uchar* bitmapBlock = (uchar*) (addr + bitmapBlockIdx * BLOCK_SIZE);
          const uchar byte = bitmapBlock[localBitmapBlockIdx];

          CHECK((inodeInUseDataBlocksBitfield[blockIdx]<1),"ERROR: direct address used more than once.\n");
          inodeInUseDataBlocksBitfield[blockIdx] = 1;

          // Validate if bitmap says data block inode is pointing to is valid
          const bool valid = (byte & bitfield) != 0;
          CHECK(valid, "ERROR: address used by inode but marked free in bitmap.\n");
          
          if (inode->type == T_DIR)
          {
            struct dirent* dir = (struct dirent*) (addr + (inode->addrs[i]) * BLOCK_SIZE);
            const uint maxDirs = BSIZE / sizeof(struct dirent);
            uint dirIdx;
            for (dirIdx = 0; dirIdx < maxDirs; ++dirIdx)
            {
              const struct dirent dirEntry = dir[dirIdx];

              // Rule out void or invalid inodes
              if (dirEntry.inum == 0 || dirEntry.inum >= numInodes) 
                continue;

              // If the directory entry is a file, incr its ref count
              struct dinode* dirInode = getInodePtr(addr, (uint) dirEntry.inum); 
              if (dirInode->type == T_FILE)
              {
                  numTimesFileIsSeenInDirectories[dirEntry.inum]++;  
              }

              // Validate that "." and ".." dirs exist
              if (selfValidated == false && strcmp(dirEntry.name, ".") == 0)
              {
                const bool valid = dirEntry.inum == inodeNum;
                CHECK(valid, "ERROR: directory not properly formatted.\n");
                selfValidated = true;
              }
              else if (parentValidated == false && strcmp(dirEntry.name, "..") == 0)
              {
                parentValidated = true;
              }
              else
              {
                // this dir is in use by an inode
                inodeInUseDirectoryBitfield[dirEntry.inum]++;
              }
            }
            // Validate parent's existance and self's correctness
            const bool dirValid = selfValidated && parentValidated;
            CHECK(dirValid, "ERROR: directory not properly formatted.\n");
          }
        }

        // Validate parent's existance and self's correctness
        //const bool dirValid = selfValidated && parentValidated;
        //CHECK(dirValid, "ERROR: directory not properly formatted.\n");

        if (!doneProcessing)
        {
          const uint indirectBlockIdx = inode->addrs[NDIRECT];
          if (indirectBlockIdx != 0)
          {
            // Validate if in range
            CHECK((indirectBlockIdx < numBlocks), "ERROR: bad direct address in inode.\n");
            CHECK((inodeInUseDataBlocksBitfield[indirectBlockIdx]<1),"ERROR: indirect address used more than once.\n");
            inodeInUseDataBlocksBitfield[indirectBlockIdx] = 1;

            const uint* indirectBlock = (uint*)(addr + indirectBlockIdx * BLOCK_SIZE);
            uint i;
            for (i = 0; i < NINDIRECT; ++i)
            {
              const uint blockIdx = indirectBlock[i];

              if (blockIdx == 0)
              {
                break;
              }

              // Validate if in range
              CHECK((blockIdx < numBlocks), "ERROR: bad indirect address in inode.\n");
              
              const uint bitmapBlockIdx = BBLOCK(blockIdx, numInodes);
              const uint localBitmapBlockIdx = (blockIdx % BPB) / 8; // can only read min 1 byte at a time, so divide by 8 and check against bitmask
              const uchar bitfield = 1 << (blockIdx % 8);
              const uchar* bitmapBlock = (uchar*) (addr + bitmapBlockIdx * BLOCK_SIZE);
              const uchar byte = bitmapBlock[localBitmapBlockIdx];

              // printf("blockIdx: %u\n", blockIdx);
              // printf("bitmapBLockIdx: %u\n", bitmapBlockIdx);
              // printf("localBitmapBlockIdx: %u\n", localBitmapBlockIdx);
              // printf("bitfield: ");
              // printByte(bitfield);
              // printf("bitmap: ");
              // printByte(byte);
              CHECK((inodeInUseDataBlocksBitfield[blockIdx]<1),"ERROR: indirect address used more than once.\n");
              inodeInUseDataBlocksBitfield[blockIdx] = 1;

              // Validate if bitmap says data block inode is pointing to is valid
              const bool valid = (byte & bitfield) != 0;
              CHECK(valid, "ERROR: address used by inode but marked free in bitmap.\n");

              // Test Case 4, ... - Directory Integrity (badfmt)
              {
                if ( inode->type == T_DIR )
                {
                  struct dirent* dir = (struct dirent*) (addr + blockIdx * BLOCK_SIZE);

                  bool selfCorrect, parentCorrect = false;
                  const uint maxDirs = BSIZE / sizeof(struct dirent);

                  //printf("Maximum maxDirs considering here is: %d",maxDirs);
                  uint dirIdx;
                  for (dirIdx = 0; dirIdx < maxDirs ; ++dirIdx) //&& (!selfCorrect || !parentCorrect)
                  {
                    const struct dirent dirEntry = dir[dirIdx];

                    if (dirEntry.inum == 0) 
                      continue;

                    if (strcmp(dirEntry.name, ".") == 0)
                    {
                      // Validate if self link exists and is valid
                      const bool valid = dirEntry.inum == inodeNum;
                      CHECK(valid, "ERROR: directory not properly formatted.\n");
                      selfCorrect = true;
                    }
                    
                    else if (strcmp(dirEntry.name, "..") == 0)
                    {
                      parentCorrect = true;
                    }
                    else{//printf("doing some printing &s %d",dirEntry.name,dirEntry.inum);
                      inodeInUseDirectoryBitfield[dirEntry.inum]++;
                    }

                    // Test Case 11 - ...
                    if (dirEntry.inum < numInodes)
                    {
                      struct dinode* dirInode = getInodePtr(addr, dirEntry.inum);
                      if (dirInode->type == T_FILE)
                      {
                        numTimesFileIsSeenInDirectories[dirEntry.inum]++;  
                      }
                    }
                  }

                  // Validate parent's existance and self's correctness
                  CHECK(selfCorrect && parentCorrect, "ERROR: directory not properly formatted.\n");
                }
              }
            }
          }
        }
      }
    }
  }

  // Test Case 6 - Data Block Bitmap Integrity (mrkused)
  {
    uint i;
    for (i = rootInode->addrs[0]; i < numBlocks; ++i)
    {
      const uint bitmapBlockIdx = BBLOCK(i, numInodes);
      const uint localBitmapBlockIdx = (i % BPB) / 8;
      const uchar* bitmapBlock = (uchar*) (addr + bitmapBlockIdx * BLOCK_SIZE);
      const uchar byte = bitmapBlock[localBitmapBlockIdx];
      const bool markedInBitmap = (byte >> (i % 8)) & 1;

      if (markedInBitmap)
      {
        const bool valid = inodeInUseDataBlocksBitfield[i];
        CHECK(valid, "ERROR: bitmap marks block in use but it is not in use.\n");
      }
    }
  }

  // Test Case 9,10,11,12 - Data Block Bitmap Integrity (mrkused) numInodes
  {
    uint i;
    for (i = ROOTINO + 1; i < numInodes; ++i)
    {
      struct dinode* inode = getInodePtr(addr, i); 
      if(inode->type!=0){
        if(inodeInUseDirectoryBitfield[i] == 0)
        {
          CHECK(false,"ERROR: inode marked use but not found in a directory.\n");
        }
        else if(inode->type == T_FILE)
        {
          //const uint ourRefCount = numTimesFileIsSeenInDirectories[i];
          const bool valid = inode->nlink == numTimesFileIsSeenInDirectories[i];
          if (valid == false)
          {
            CHECK(valid,"ERROR: bad reference count for file.\n");
          }
        }
        else if(inodeInUseDirectoryBitfield[i]>1){
          CHECK(false,"ERROR: directory appears more than once in file system.\n");
        }
      }else{
        CHECK((inodeInUseDirectoryBitfield[i]==0),"ERROR: inode referred to in directory but marked free.\n");
      }
    }
  }



  // get the address of root dir 
  de = (struct dirent *) (addr + (dip[ROOTINO].addrs[0])*BLOCK_SIZE);
  //printf("%u\n", dip[ROOTINO].addrs[0]);

  // print the entries in the first block of root dir 

  n = dip[ROOTINO].size/sizeof(struct dirent);
  for (i = 0; i < n; i++,de++){
 	//printf(" inum %d, name %s ", de->inum, de->name);
  	//printf("inode  size %d links %d type %d \n", dip[de->inum].size, dip[de->inum].nlink, dip[de->inum].type);
  }
  exit(0);

}

